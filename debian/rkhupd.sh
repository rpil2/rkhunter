#!/bin/sh
set -e

CONFIG=""
for f in /etc/rkhunter/rkhunter.conf /etc/rkhunter/rkhunter.conf.local /etc/rkhunter/rkhunter.d/*.conf; do
		 [ -f "$f" ] && CONFIG="$CONFIG $f"
done
[ -z "$CONFIG" ] && exit 0

if ! grep -qsE '^DISABLE_TESTS=.*(hashes.*attributes|attributes.*hashes|properties)' $CONFIG || \
     grep -qsE '^ENABLE_TESTS=.*(hashes|attributes|properties)' $CONFIG; then
		rkhunter --propupd --nolog
fi
