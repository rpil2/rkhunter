rkhunter
--------

* RKHUNTER FAQ
  The Rootkit Hunter FAQ can be found at:
  https://sourceforge.net/p/rkhunter/rkh_code/ci/master/tree/files/FAQ

* CONFIGURATION FILES
  It is possible to put configuration changes into any of
  * /etc/rkhunter/rkhunter.conf
  * /etc/rkhunter/rkhunter.conf.local
  * files in /etc/rkhunter/rkhunter.d
     (the name of files in /etc/rkhunter/rkhunter.d must not include
     whitespace and must end in '.conf')

  These are checked in order: For options allowed only once, the last
  setting seen is used, and for options allowed more than once, all
  settings are combined.

  The Debian package only ships /etc/rkhunter/rkhunter.conf so you
  will need to create the others (including the directory
  /etc/rkhunter/rkhunter.d) if you want to use them.  On purge only
  files shipped by Debian are deleted
  

* FALSE POSITIVES
  
  If openssh-server is installed, rkhunter will check that
  /etc/ssh/sshd_config has 'good' settings for PermitRootLogin and Protocol.
  If these are not set in that file then rkhunter will produce warnings
  that the defaults may not be secure. If you do not want to include
  those options in sshd_config you can Set ALLOW_ROOT_USER=unset
  and ALLOW_SSH_PROT_V1=2 in the rkhunter configuration to avoid warnings.
  Note that rkhunter does not understand that sshd_config can load other
  files via 'Include'.

  Some Debian packages implement 'standard' commands as scripts, which
  are flagged by rkhunter unless you put the path in the SCRIPTWHITELIST
	option i  rkhunter's configuration. Packages affected include:
    * adduser - /usr/sbin/adduser (y)
    * debianutils - /usr/bin/which  (y)
    * grep - /usr/bin/egrep, /usr/bin/egrep (y)
    * ifplugd - /usr/sbin/ifstatus
    * libwww-perl - /usr/sbin/lwp-request
    * libc-bin - /usr/bin/ldd (y)
  Items labelled (y) are whitelisted by default as the packages are
  high priority. rkhunter gives an error if a whitelisted file does
  not exist so we cannot add all false positives to the default
  configuration.

  Some Debian packages include 'hidden' files and directories which
  are flagged by rkhunter:
    * java-common: creates /etc/.java/ - many other java packages
      use this 'hidden' directory
  The following used to be common, but are no longer created in
  default installations:  
    * /dev/.initramfs - used by initramfs-tools for temporary
      filesystems during boot.
    * /dev/.static/, /dev/.udev & /dev/.udevdb/ - used by old udev packages
  You can use the ALLOWHIDDENFILE and ALLOWHIDDENDIR options in
  /etc/rkhunter/rkhunter.conf (or the other config files explained
  above) to prevent rkhunter warning about such files.

  The following Debian packages trigger false alarms in rkhunter:
    * slice: /usr/bin/slice causes a false alarm about an RH-Sharpe
      rootkit
    * sash: creates a sashroot account with UID=0 which rkhunter flags
      as suspicious.  You can avoid this warning by setting the
      UID0_ACCOUNTS option in a configuration file (see above)
    * hdparm: the string "hdparm" found in /etc/init.d/hdparm leads to
      rkhunter warning about a possible Xzibit rootkit. Use the
      RTKT_FILE_WHITELIST option to avoid the warning
    * IRC daemons trigger warnings about a possible rogue IRC bot. You
      can whitelist the TCP port 6667, or, better, tell rkhunter to
      trust the executable, using the PORT_WHITELIST option, eg:
      PORT_WHITELIST="/usr/bin/znc"

* HASH CHECKS
  rkhunter can check that files installed by Debian packages have the
  expected checksums, by comparing their properties to a database and
  reporting changes.  By default, all such hash checks are ENABLED in
  the daily cron job.

  Whenever a Debian packages is installed or updated (for example due
  to security issues), you will need to update the database (stored at
  /var/lib/rkhunter/db/rkhunter.dat), or changed files will be flagged
  as suspicious. You can update the database manually or it can be
  updated automatically.

  To enable automatic database updates, run
  # dpkg-reconfigure rkhunter

  (this enables /etc/apt.conf.d/90rkhunter which will runs after
  packages are installed, removed or upgraded).

  To update the database manually, run
  # rkhunter --propupd
  
  SECURITY WARNING: The database update (whether atomatic or by manual invocattion of 'rkhunter --propupd')
    puts data from the currently installed files into the database.

    There is therefore a race condition: an attacker could replace a
    file immediately after it is installed, but before the database is
    updated, and then rkhunter will put the 'bad' checksums in its
    database and report everything is fine.
  
    Ultimately, the security of a system depends on the administrator and cannot be guaranteed by
    a simple took like rkhunter.
    On a highly protected machine, you should consider disabling the automatic database update and
    running 'rkhunter --propupd' only when you have verified the installed files are 'correct' by
    some other means.

  To disable the hash checkes, add either the 'hashes' and 'attributes' tests or the group 'properties'
  to the DISABLE_TESTS option in rkhunter's configuration.


* WEEKLY DATABASE UPDATES
  rkhunter has the ability to update some data automatically. This is disabled by default due to
  security concerns over packages downloading potentially untrusted data. 
  See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=765895 for details.

  If you want to re-enable it you will need to:
  - install one of wget, curl, (e)links, lynx or GET to download the updates.
  - reconfigure rkhunter to activate the automatic weekly database update:
    # dpkg-reconfigure rkhunter

* SKDET
  rkhunter can run a 3rd party tool, skdet.  skdet is not in Debian as skdet is unmaintained, has an
  unknown license (so cannot be included in Debian) and its website is no longer available.
	An archived copy may be available at available at http://www.xs4all.nl/~dvgevers/ - USE AT YOUR OWN RISK

 -- Debian Security Tools <team+pkg-security@tracker.debian.org>, Sun, 31 Oct 2021 12:28:02 +0000
